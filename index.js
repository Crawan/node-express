const express = require('express');
const http = require('http');
const morgan = require('morgan');
const bodyParser = require('body-parser');


const dishRouter = require('./routes/dishRouter');

const promoRouter = require('./routes/promoRouter');

const leaderRouter = require('./routes/leaderRouter');

const hostname = 'localhost';
const port = 3000;

const app = express();//express node modules ...
app.use(morgan('dev'));//To print additional information to the screen..

//moutn the the dishRouter..
app.use('/dishes', dishRouter);
app.use('/promotions', promoRouter);
app.use('/leaders', leaderRouter);

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());





//to setup our server ...

app.use((req, res, next) => {
    
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.end('<html><body><h1>This is Express Server</h1></body></html>');
});

const server = http.createServer(app);

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port} `);
});